<h1>EvilNite</h1>

<h3>Telegram bot for chat administration</h3>

## Description⚡

This bot is being written as a replacement for Rosa and Shmalala. Here is the list of functionality (current, and future):

- [x] Ban/Mut/Kick users
- [ ] Greeting users
- [ ] Raid protection
- [ ] Manage chat rules:
  - [ ] Sending rules to the /rules command
  - [ ] Setting rules on the /setrules command
- [ ] Modules:
  - [x] Message translation
  - [ ] Search by:
    - [ ] Wikipedia
    - [ ] ArchLinux Wiki
    - [ ] Gentoo Wiki
  - [ ] Mini-games
  - [x] Anti-spam system
    
## How to install✨

Installation instructions for EvilNite:

1. Download the source code of the project, either via Zip after unzipping it, or with the command `git clone --depth 1 https://gitlab.com/Xeronix/EvilNite`

2. Install and configure PostgreSQL database, I will not describe how to do it, it is better to search in Google

3. Install the .NET Core SDK on your computer if it is not already installed. You can download it from the official website: https://dotnet.microsoft.com/download

4. Open the terminal or command prompt on your computer.

5. Enter the following command in the terminal or command prompt:

    * `dotnet build --configuration Release`

6. Wait for the build and then run the executable file at the path: `CurrentFolder/EvilNite/bin/Release/net7.0`.

## Data for Training Anti-Spam System

For the bot, I started creating a table with Spam/Ham messages independently (not without the help of my admins and some Telegram users). In return, I would like to ask the following: if you have developed a larger model based on mine, please send it to me via a Pull Request. Thank you in advance!

## Community

My channel on Telegram: https://t.me/kirmozor

There is also a mirror in the Mastodon: https://mastodon.social/@kirmozor

We also have a chat room, the link is in the attached message of the channel😁

## Sponsors
This project is sponsored by JetBrains and GitLab. Thanks to them, I have a license for the IDE and all the features of GitLab Ultimate🙂

<img src="https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.svg" width="100" height="100">
<img src="https://resources.jetbrains.com/storage/products/company/brand/logos/Rider.svg" width="100" height="100">
<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-100.svg" width="100" height="100">

## Helping the channel and development

If you want to help this project as well as my channels, the details are below:
* Ton - UQAt_Dl_TAOCJVe09oXMS1XeMHz2DXX2JrCdA0cSyJaCRd1Q
* Btc - 1CNBs7r8NpwvZXanw5fx7h6ggrBwGzZVkS
* Tether (TRC20) - TDskFXZUFG3UCvAmaCxkdtyudWzYs5t7yf
* Tether (BEP20) - 0x51d5a4530670d81a2382edf266f4d5975e83d377
* BNB (BEP20) - 0x51d5a4530670d81a2382edf266f4d5975e83d377
* Visa - 4400430240807740
