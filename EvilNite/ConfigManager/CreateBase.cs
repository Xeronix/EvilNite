using EvilNite.ConfigManager.Config;
using EvilNite.Core;
using Telegram.Bot;

namespace EvilNite.ConfigManager;

/// <summary>
/// This class has a method to create a base config and validate the data from the user
/// </summary>
public class CreateBase
{
    private static readonly BotConfig BotConfig = new BotConfig();
    
    /// <summary>
    /// This method creates a config and checks the input data at the same time
    /// </summary>
    public void Config()
    {
        LoggerSetup.Log.Information(Localization.Logging.CreateBase_RunConfig);
        
        Console.WriteLine(Localization.Core.Launch.CreateConfiguration_Greetings);
        while (true)
        {
            Console.WriteLine(Localization.Core.Launch.CreateConfiguration_EnterToken);
            BotConfig.BotToken = Console.ReadLine();

            if (CheckToken(BotConfig.BotToken))
                break;
            else
                Console.WriteLine(Localization.ConfigManager.CreateBase.ProblemToken);
        }
        Console.WriteLine(Localization.ConfigManager.CreateBase.EnterIDChat);
        BotConfig.ChatId = Console.ReadLine();

        while (true)
        {
            Console.WriteLine(Localization.Core.Launch.CreateConfiguration_DatabaseHost);
            BotConfig.HostDatabase = Console.ReadLine();

            while (true)
            {
                try
                {
                    Console.WriteLine(Localization.Core.Launch.CreateConfiguration_DatabasePort);
                    BotConfig.PortDatabase = Convert.ToInt32(Console.ReadLine());
                    break;
                }
                catch (FormatException exception)
                {
                    Helper.OutputErrorWithEx(exception, Localization.ConfigManager.CreateBase.IncorrectPort);
                }
            }
            
            Console.WriteLine(Localization.Core.Launch.CreateConfiguration_DatabaseName);
            BotConfig.NameDatabase = Console.ReadLine();
            Console.WriteLine(Localization.Core.Launch.CreateConfiguration_DatabaseUsername);
            BotConfig.UserAccessDatabase = Console.ReadLine();
            Console.WriteLine(Localization.Core.Launch.CreateConfiguration_DatabasePassword);
            BotConfig.PasswordUser = Console.ReadLine();

            break;
        }

        ReadWriteConfig.ConfigModel = BotConfig;
        ReadWriteConfig readWriteConfig = new ReadWriteConfig();
        readWriteConfig.WriteConfig();
        
        Console.Clear();
    }

    /// <summary>
    /// This method checks the token from Telegram bot for correctness
    /// </summary>
    /// <param name="token">A token from a Telegram bot that needs to be verified</param>
    /// <returns>True if the token is valid and vice versa</returns>
    private bool CheckToken(string token)
    {
        LoggerSetup.Log.Information(Localization.Logging.CreateBase_RunCheckToken);
        LoggerSetup.Log.Debug(Localization.Logging.Text, token);
        
        var checkTokenBot = new TelegramBotClient(token);
        bool isTokenValid = checkTokenBot.BotId != null;
        
        LoggerSetup.Log.Debug(Localization.Logging.CreateBase_CheckTokenValid);
        return isTokenValid;
    }
}