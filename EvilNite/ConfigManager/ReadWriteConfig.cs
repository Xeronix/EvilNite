using EvilNite.ConfigManager.Config;
using Tomlyn;

namespace EvilNite.ConfigManager;

/// <summary>
/// This class deals with loading the configuration and writing it to the config.
/// </summary>
public class ReadWriteConfig
{
    public static BotConfig ConfigModel;
    /// <summary>
    /// Method for reading the config from the file ~/.local/share/EvilNite/config.toml and further serialized into the BotConfig class
    /// </summary>
    public void ReadConfig()
    {
        LoggerSetup.Log.Information(Localization.Logging.ReadWriteConfig_RunReadConfig);
        
        var tomlFile = File.ReadAllText(GlobalConfig.PathToConfApp);
        LoggerSetup.Log.Debug(Localization.Logging.Result, tomlFile);
        
        ConfigModel = Toml.ToModel<BotConfig>(tomlFile);
    }

    /// <summary>
    /// This method deserializes the class with the BotConfig configuration into a string and then writes it to the config.toml file
    /// </summary>
    public void WriteConfig()
    {
        LoggerSetup.Log.Information(Localization.Logging.ReadWriteConfig_RunWriteConfig);
        var tomlOut = Toml.FromModel(ConfigModel);
        LoggerSetup.Log.Debug(Localization.Logging.ReadWriteConfig_WriteConfigResultConvert, tomlOut);
        
        Directory.CreateDirectory($"{GlobalConfig.UserFolder}/.local/share/EvilNite");
        File.WriteAllText(GlobalConfig.PathToConfApp, tomlOut);
        
        LoggerSetup.Log.Information(Localization.Logging.ReadWriteConfig_WriteConfigPath, GlobalConfig.PathToConfApp);
    }
}