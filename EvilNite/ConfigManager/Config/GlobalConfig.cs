namespace EvilNite.ConfigManager.Config;

/// <summary>
/// Global class with bot configuration
/// </summary>
public class GlobalConfig
{
    /// <summary>
    /// Path to the user's folder (not to write a long string to get the user's folder each time)
    /// </summary>
    public static string UserFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

    /// <summary>
    /// Full path to the bot configuration file
    /// </summary>
    public static string PathToConfApp = Path.Combine($"{UserFolder}/.local/share/EvilNite/config.toml");
}