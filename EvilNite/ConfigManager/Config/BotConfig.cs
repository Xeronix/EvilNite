namespace EvilNite.ConfigManager.Config;

/// <summary>
/// This class will unload the Telegram bot configuration from config.toml and use it in the bot
/// </summary>
public class BotConfig
{
    /// <summary>
    /// A token for a Telegram bot
    /// </summary>
    public string? BotToken { get; set; }
    
    /// <summary>
    /// Database host
    /// </summary>
    public string? HostDatabase { get; set; }
    
    /// <summary>
    /// The port on which the database is running
    /// </summary>
    public int PortDatabase { get; set; }
    
    /// <summary>
    /// Database Name
    /// </summary>
    public string? NameDatabase { get; set; }
    
    /// <summary>
    /// Name of the user who has access to the database
    /// </summary>
    public string? UserAccessDatabase { get; set; }
    
    /// <summary>
    /// Password of the user who has access to the database
    /// </summary>
    public string? PasswordUser { get; set; }
    
    /// <summary>
    /// ID of the chat in which the bot will work (it is calculated for 1 chat)
    /// </summary>
    public string? ChatId { get; set; }
}