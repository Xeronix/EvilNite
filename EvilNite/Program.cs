﻿using EvilNite.Core;

namespace EvilNite;

internal abstract class Program
{
    private static async Task Main()
    {
        LoggerSetup.Log.Information(Localization.Logging.Main_StartingBot);
        // This method runs methods to create reading the Toml config and creating tables in the database
        var launch = new Launch();
        await launch.LaunchBot();
    }
}
