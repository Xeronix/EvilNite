using System.Data;
using MySql.Data.MySqlClient;

namespace EvilNite.Database;

/// <summary>
/// Base Class to Get a Database Connection
/// </summary>
public abstract class ApplicationContext
{
    private static readonly string ConnectionString = 
        $"Server={ConfigManager.ReadWriteConfig.ConfigModel.HostDatabase};" +
        $"Database={ConfigManager.ReadWriteConfig.ConfigModel.NameDatabase};" +
        $"User={ConfigManager.ReadWriteConfig.ConfigModel.UserAccessDatabase};" +
        $"Password={ConfigManager.ReadWriteConfig.ConfigModel.PasswordUser};";

    /// <summary>
    /// Method to get a database connection
    /// </summary>
    /// <returns>An object of the MySqlConnection class that can be used to manipulate the database</returns>
    public static IDbConnection GetConnection()
    {
        LoggerSetup.Log.Information(Localization.Logging.DbApplicationContext_GetConnection, ConnectionString);
        return new MySqlConnection(ConnectionString);
    }
}