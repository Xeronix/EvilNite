namespace EvilNite.Database;

public class SpamMessageMaybe
{
    public long Id { get; set; }
    public string Message { get; set; }
    public long MessageBotId { get; set; }
    public long UserId { get; set; }
}