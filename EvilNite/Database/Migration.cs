using Dapper;

namespace EvilNite.Database;

/// <summary>
/// A class to create the necessary tables in the database
/// </summary>
public class Migration 
{
    /// <summary>
    /// This is where the SQL statements that will create the necessary tables in the database are placed
    /// </summary>
    private readonly string[] _upCommand =
    [
        "CREATE TABLE IF NOT EXISTS Members (Id BIGINT PRIMARY KEY, CountMessages BIGINT);",
        "CREATE TABLE IF NOT EXISTS SpamHam (Label TEXT, Message TEXT)",
        "CREATE TABLE IF NOT EXISTS SpamMessagesMaybe (Id BIGINT PRIMARY KEY, Message TEXT, MessageBotId BIGINT, UserId BIGINT)"
    ];
    /// <summary>
    /// A method for creating tables based on the SQL statements presented in the array <see cref="_upCommand"/>
    /// </summary>
    public void Up()
    {
        using var connection = ApplicationContext.GetConnection();
        connection.Open();

        foreach (var i in _upCommand)
        {
            LoggerSetup.Log.Information(Localization.Logging.Migration_CreateTable, i);
            connection.Execute(i);
        }
            
        connection.Close();
    }
}