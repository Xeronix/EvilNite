namespace EvilNite.Database
{
    /// <summary>
    /// Represents a model for a chat member table in the database.
    /// </summary>
    public class Member
    {
        /// <summary>
        /// Gets or sets the unique identifier for the chat member.
        /// </summary>
        /// <value>
        /// The unique identifier of the chat member (ID).
        /// </value>
        public long Id { get; set; }

        /// <summary>
        /// Gets or sets the number of messages sent by the chat member.
        /// </summary>
        /// <value>
        /// The number of messages sent by the chat member.
        /// </value>
        public long CountMessages { get; set; }
    }
}