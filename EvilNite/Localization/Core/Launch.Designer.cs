﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EvilNite.Localization.Core {
    using System;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Launch {
        
        private static System.Resources.ResourceManager resourceMan;
        
        private static System.Globalization.CultureInfo resourceCulture;
        
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Launch() {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static System.Resources.ResourceManager ResourceManager {
            get {
                if (object.Equals(null, resourceMan)) {
                    System.Resources.ResourceManager temp = new System.Resources.ResourceManager("EvilNite.Localization.Core.Launch", typeof(Launch).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        internal static string CreateConfiguration_Greetings {
            get {
                return ResourceManager.GetString("CreateConfiguration_Greetings", resourceCulture);
            }
        }
        
        internal static string CreateConfiguration_EnterToken {
            get {
                return ResourceManager.GetString("CreateConfiguration_EnterToken", resourceCulture);
            }
        }
        
        internal static string CreateConfiguration_DatabaseUsername {
            get {
                return ResourceManager.GetString("CreateConfiguration_DatabaseUsername", resourceCulture);
            }
        }
        
        internal static string CreateConfiguration_DatabaseName {
            get {
                return ResourceManager.GetString("CreateConfiguration_DatabaseName", resourceCulture);
            }
        }
        
        internal static string CreateConfiguration_DatabasePassword {
            get {
                return ResourceManager.GetString("CreateConfiguration_DatabasePassword", resourceCulture);
            }
        }
        
        internal static string CreateConfiguration_DatabaseHost {
            get {
                return ResourceManager.GetString("CreateConfiguration_DatabaseHost", resourceCulture);
            }
        }
        
        internal static string CreateConfiguration_DatabasePort {
            get {
                return ResourceManager.GetString("CreateConfiguration_DatabasePort", resourceCulture);
            }
        }
    }
}
