namespace EvilNite.Modules.AntiSpamSystem;

/// <summary>
/// Represents the callback data associated with a spam message.
/// </summary>
public class CallbackData
{
    public string Type { get; set; }
    public long MessageId { get; set; }
}