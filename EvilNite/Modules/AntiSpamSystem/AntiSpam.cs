using Dapper;
using EvilNite.Core;
using EvilNite.Core.Commands;
using EvilNite.Database;
using Microsoft.ML;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;

namespace EvilNite.Modules.AntiSpamSystem;

public static class AntiSpam
{
    private static PredictionEngine<SpamInput, SpamPrediction>? _predictionEngine;
    
    /// <summary>
    /// Evaluates a message for spam and takes appropriate actions such as muting the user and logging the message for admin review.
    /// </summary>
    /// <param name="botClient">Telegram bot client</param>
    /// <param name="update">Update object containing the message to evaluate</param>
    /// <returns>Returns true if the message was classified as spam and actions were taken, false otherwise</returns>
    public static async Task<bool> MessageEvaluation(ITelegramBotClient botClient, Telegram.Bot.Types.Update update)
    {
        LoggerSetup.Log.Information(Localization.Logging.AntiSpam_MethodStart);
        
        try
        {
            string text = update.Message.Text;
            int messageId = update.Message.MessageId;
            long userId = update.Message.From.Id;
            
            LoggerSetup.Log.Debug(Localization.Logging.AntiSpam_InputParameters, text, messageId, userId);

            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_GetCountMessages, userId);
            var countMessages = await GetCountMessages(userId);

            if (countMessages <= 20)
            {
                LoggerSetup.Log.Information(Localization.Logging.AntiSpam_ClassifyMessage);
                if (ClassifyMessage(text) == "spam")
                {
                    LoggerSetup.Log.Information(Localization.Logging.AntiSpam_MessageClassifiedAsSpam);
                    
                    InlineKeyboardButton thisSpam = CreateButton("\u26d4\ufe0fСпам", "SpamMessage", messageId);
                    InlineKeyboardButton notSpam = CreateButton("\ud83d\udc40Не спам", "NotSpamMessage", messageId);
                    InlineKeyboardButton showMessage = CreateButton("\ud83d\udcacПоказать сообщение", "ShowSpamMessage", messageId);

                    InlineKeyboardMarkup keyboard = new InlineKeyboardMarkup(new[]
                    {
                        new[] { thisSpam, notSpam },
                        new[] { showMessage }
                    });

                    LoggerSetup.Log.Information(Localization.Logging.AntiSpam_SendMessageToAdmins);
                    var messageBot = await botClient.SendTextMessageAsync(update.Message.Chat.Id,
                        $"Мне кажется это спамер (@{update.Message.From.Username})... В мут! Админы потом разберутся",
                        replyMarkup: keyboard);

                    LoggerSetup.Log.Information(Localization.Logging.AntiSpam_InsertSpamMessageToDb, messageId);
                    using (var connection = ApplicationContext.GetConnection())
                    {
                        connection.Open();

                        const string spamMessageMaybeInsert = "INSERT INTO SpamMessagesMaybe VALUES (@Id, @Message, @MessageBotId, @UserId)";
                        await connection.ExecuteAsync(spamMessageMaybeInsert, new { Id = messageId, Message = text, MessageBotId = messageBot.MessageId, UserId = userId });
                    }

                    LoggerSetup.Log.Information(Localization.Logging.AntiSpam_DeleteOriginalMessage);
                    await botClient.DeleteMessageAsync(update.Message.Chat.Id, messageId);

                    LoggerSetup.Log.Information(Localization.Logging.AntiSpam_MuteUser);
                    Mute muteObject = new Mute();
                    await muteObject.MuteMember(botClient, new[] { "", "24h" }, update.Message.Chat.Id, userId);

                    LoggerSetup.Log.Information(Localization.Logging.AntiSpam_MessageEvaluationComplete);
                    return true;
                }
                else
                {
                    LoggerSetup.Log.Information(Localization.Logging.AntiSpam_MessageNotSpam);
                    return false;
                }
            }

            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_UserMessageCountExceedsThreshold);
            return false;
        }
        catch (Exception ex)
        {
            Helper.OutputErrorWithEx(ex);
            throw;
        }
    }


    /// <summary>
    /// Processes a message identified as spam, removes it from the database, adds it to the spam category, deletes the message from the chat, and bans the user.
    /// </summary>
    /// <param name="botClient">Telegram bot client</param>
    /// <param name="messageId">ID of the message to process</param>
    public static async Task ThisSpamMessage(ITelegramBotClient botClient, int messageId)
    {
        LoggerSetup.Log.Information(Localization.Logging.AntiSpam_RunThisSpamMessage);
        LoggerSetup.Log.Debug(Localization.Logging.MessageId, messageId);
        
        try
        {
            var spamMessageMaybe = await GetAndRemoveMessageFromDb(messageId);

            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_AddSpamMessageDbDeleteMessage);
            await AddSpamHamMessage(true, spamMessageMaybe.Message);
            await botClient.DeleteMessageAsync(ConfigManager.ReadWriteConfig.ConfigModel.ChatId, Convert.ToInt32(spamMessageMaybe.MessageBotId));

            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_BanUser, spamMessageMaybe.UserId);
            Ban banObject = new Ban();
            await banObject.BanMember(botClient, spamMessageMaybe.UserId);
        }
        catch (Exception ex)
        {
            Helper.OutputErrorWithEx(ex, string.Format(Localization.Logging.AntiSpam_ThisSpamMessageError, messageId));
            throw;
        }
    }

    /// <summary>
    /// Processes a message identified as not spam, removes it from the database, adds it to the ham category, deletes the message from the chat, and unmutes the user.
    /// </summary>
    /// <param name="botClient">Telegram bot client</param>
    /// <param name="messageId">ID of the message to process</param>
    public static async Task ThisNotSpamMessage(ITelegramBotClient botClient, int messageId)
    {
        LoggerSetup.Log.Information(Localization.Logging.AntiSpam_RunThisNotSpamMessage);
        LoggerSetup.Log.Debug(Localization.Logging.MessageId, messageId);
        
        try
        {
            var spamMessageMaybe = await GetAndRemoveMessageFromDb(messageId);

            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_AntiSpam_AddHamMessageDbDeleteMessage);
            await AddSpamHamMessage(false, spamMessageMaybe.Message);
            await botClient.DeleteMessageAsync(ConfigManager.ReadWriteConfig.ConfigModel.ChatId, Convert.ToInt32(spamMessageMaybe.MessageBotId));

            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_UnmutingUser, spamMessageMaybe.UserId);
            Mute muteObject = new Mute();
            await muteObject.UnmuteMember(botClient, Convert.ToInt64(ConfigManager.ReadWriteConfig.ConfigModel.ChatId), spamMessageMaybe.UserId);
        }
        catch (Exception ex)
        {
            Helper.OutputErrorWithEx(ex);
            throw;
        }
    }

    /// <summary>
    /// Retrieves and removes a message from the SpamMessagesMaybe table in the database.
    /// </summary>
    /// <param name="messageId">ID of the message to retrieve and remove</param>
    /// <returns>The removed SpamMessageMaybe object</returns>
    private static async Task<SpamMessageMaybe> GetAndRemoveMessageFromDb(int messageId)
    {
        LoggerSetup.Log.Information(Localization.Logging.AntiSpam_RunGetAndRemoveMessageFromDb);
        LoggerSetup.Log.Debug(Localization.Logging.MessageId, messageId);

        using (var connection = ApplicationContext.GetConnection())
        {
            connection.Open();

            const string selectSpamMessageQuery = "SELECT Id, Message, MessageBotId, UserId FROM SpamMessagesMaybe WHERE Id = @MessageId";
            var spamMessageMaybe = await connection.QuerySingleOrDefaultAsync<SpamMessageMaybe>(selectSpamMessageQuery, new { MessageId = messageId });

            if (spamMessageMaybe == null)
            {
                LoggerSetup.Log.Warning(Localization.Logging.AntiSpam_MessageIdNotFound, messageId);
                throw new Exception(string.Format(Localization.Logging.AntiSpam_MessageIdNotFound, messageId));
            }

            const string removeSpamMessage = "DELETE FROM SpamMessagesMaybe WHERE Id = @MessageId";
            await connection.ExecuteAsync(removeSpamMessage, new { MessageId = messageId });

            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_MessageIdDeleteDb, messageId);

            return spamMessageMaybe;
        }
    }
        
    /// <summary>
    /// Sends a spam message to PM that causes the user to mute
    /// </summary>
    /// <param name="botClient">bot object</param>
    /// <param name="userId">ID of the user to whom the message is sent</param>
    /// <param name="messageId">Message ID</param>
    public static async Task ShowSpamMessage(ITelegramBotClient botClient, long userId, long messageId)
    {
        LoggerSetup.Log.Information(Localization.Logging.AntiSpam_RunShowSpamMessage);

        using (var connection = ApplicationContext.GetConnection())
        {
            try
            {
                connection.Open();

                const string selectSpamMessageQuery = "SELECT Id, Message, UserId FROM SpamMessagesMaybe WHERE Id = @MessageId";
                LoggerSetup.Log.Information(Localization.Logging.ExecutingSqlQuery, selectSpamMessageQuery);

                var selectSpamMessage = await connection.QuerySingleOrDefaultAsync<SpamMessageMaybe>(selectSpamMessageQuery, new { MessageId = messageId });

                if (selectSpamMessage != null)
                {
                    LoggerSetup.Log.Information(Localization.Logging.AntiSpam_FoundIdMessage, messageId);
                    await botClient.SendTextMessageAsync(userId, string.Format(Localization.Logging.AntiSpam_SpamMessage, selectSpamMessage.Message));
                }
                else
                    LoggerSetup.Log.Warning(Localization.Logging.AntiSpam_NotFoundIdSpamMessage, messageId);
            }
            catch (Exception e)
            {
                Helper.OutputErrorWithEx(e, string.Format(Localization.Logging.AntiSpam_ErrorSendSpamMessage, userId));
                await botClient.SendTextMessageAsync(ConfigManager.ReadWriteConfig.ConfigModel.ChatId,
                    Localization.Logging.AntiSpam_ErrorSendSpamMessageText);
            }
        }
    }

    /// <summary>
    /// This method adds the message to the spam or ham category
    /// </summary>
    /// <param name="isSpam">If spam is true, ham is false</param>
    /// <param name="message">Message</param>
    public static async Task AddSpamHamMessage(bool isSpam, string message)
    {
        LoggerSetup.Log.Information(Localization.Logging.AntiSpam_RunAddSpamHamMessage);
        LoggerSetup.Log.Debug(Localization.Logging.AntiSpam_SpamHamMessageDebugParam, isSpam, message);

        using (var connection = ApplicationContext.GetConnection())
        {
            try
            {
                connection.Open();

                const string addSpamMessage = "INSERT INTO SpamHam VALUES (@Label, @Message)";
                await connection.ExecuteAsync(addSpamMessage, new { Label = isSpam ? "spam" : "ham", Message = message });

                LoggerSetup.Log.Information(Localization.Logging.AntiSpam_AddMessageToDb, isSpam ? "spam" : "ham");
            }
            catch (Exception e)
            {
                Helper.OutputErrorWithEx(e, string.Format(Localization.Logging.AntiSpam_ErrorAddMessageToDb, isSpam ? "spam" : "ham"));
                throw;
            }
        }
    }


    /// <summary>
    /// A method for training a neural network for further use in a bot
    /// </summary>
    public static void TrainingSpamDetector()
    {
        LoggerSetup.Log.Information(Localization.Logging.AntiSpam_RunTrainingSpamDetector);

        try
        {
            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_LoadDataForTraining);
            var spamData = GetSpamData();
            MLContext mlContext = new MLContext();

            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_CreatePipeline);
            var data = mlContext.Data.LoadFromEnumerable(spamData);
            var dataProcessPipeline = mlContext.Transforms.Conversion.MapValueToKey("Label", "Label")
                .Append(mlContext.Transforms.Text.FeaturizeText("FeaturesText", new Microsoft.ML.Transforms.Text.TextFeaturizingEstimator.Options
                {
                    WordFeatureExtractor = new Microsoft.ML.Transforms.Text.WordBagEstimator.Options { NgramLength = 2, UseAllLengths = true },
                    CharFeatureExtractor = new Microsoft.ML.Transforms.Text.WordBagEstimator.Options { NgramLength = 3, UseAllLengths = false },
                    Norm = Microsoft.ML.Transforms.Text.TextFeaturizingEstimator.NormFunction.L2,
                }, "Message"))
                .Append(mlContext.Transforms.CopyColumns("Features", "FeaturesText"))
                .AppendCacheCheckpoint(mlContext);

            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_StartTrainingModel);
            var trainer = mlContext.MulticlassClassification.Trainers.OneVersusAll(
                mlContext.BinaryClassification.Trainers.AveragedPerceptron(
                    labelColumnName: "Label", 
                    numberOfIterations: 10, 
                    featureColumnName: "Features", 
                    learningRate: 0.001f), 
                labelColumnName: "Label")
                .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel", "PredictedLabel"));
            
            var trainingPipeLine = dataProcessPipeline.Append(trainer);

            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_StartProcessTrainingModel);
            var model = trainingPipeLine.Fit(data);

            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_CreatePredictionEngine);
            _predictionEngine = mlContext.Model.CreatePredictionEngine<SpamInput, SpamPrediction>(model);
            
            LoggerSetup.Log.Information(Localization.Logging.AntiSpam_CompleteTrainingAi);
        }
        catch (Exception ex)
        {
            Helper.OutputErrorWithEx(ex);
            throw;
        }
    }

    
    /// <summary>
    /// Receives all spam ham messages for neural network training
    /// </summary>
    /// <returns>Spam Ham Messages</returns>
    private static List<SpamInput> GetSpamData()
    {
        LoggerSetup.Log.Information("Запущен метод для получения spam ham сообщений из бд");
        using (var connection = ApplicationContext.GetConnection())
        {
            connection.Open();

            var query = "SELECT Label, Message FROM SpamHam";
            var spamData = connection.Query<SpamInput>(query).ToList();

            return spamData;
        }
    }

    /// <summary>
    /// This method checks whether a message is spam or not
    /// </summary>
    /// <param name="message">Message Text</param>
    /// <returns>spam - if the message is spam, ham - if not</returns>
    private static string ClassifyMessage(string message)
    {
        LoggerSetup.Log.Information(Localization.Logging.AntiSpam_RunClassifyMessage);
        LoggerSetup.Log.Debug(Localization.Logging.Text, message);
        
        var input = new SpamInput { Message = message };
        var prediction = _predictionEngine?.Predict(input);
        string isSpam = prediction.isSpam;
        
        LoggerSetup.Log.Debug(Localization.Logging.Result, isSpam);
        return isSpam;
    }
    
    /// <summary>
    /// This method creates a button for the InlineKeyboard
    /// </summary>
    /// <param name="label">Button Text</param>
    /// <param name="type">Button Type</param>
    /// <param name="messageId">Message ID</param>
    /// <returns>An object of type InlineKeyboard with your data</returns>
    private static InlineKeyboardButton CreateButton(string label, string type, long messageId)
    {
        LoggerSetup.Log.Information(Localization.Logging.AntiSpam_RunCreateButton);
        
        var callbackData = new CallbackData
        {
            Type = type,
            MessageId = messageId
        };

        string json = JsonConvert.SerializeObject(callbackData);
        LoggerSetup.Log.Debug(Localization.Logging.AntiSpam_ButtonCreated, json);
        return InlineKeyboardButton.WithCallbackData(label, json);
    }
    
    /// <summary>
    /// This method retrieves the number of user messages
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <returns>Number of messages in the long type</returns>
    private static async Task<long> GetCountMessages(long userId)
    {
        LoggerSetup.Log.Information(Localization.Logging.AntiSpam_RunGetCountMessages);
        LoggerSetup.Log.Debug(Localization.Logging.UserId, userId);
        
        using (var connection = ApplicationContext.GetConnection())
        {
            connection.Open();
            
            const string countMessagesQuery = "SELECT CountMessages FROM Members WHERE Id = @Id";
            var countMessages = await connection.ExecuteScalarAsync<long>(countMessagesQuery, new { Id = userId });
            
            LoggerSetup.Log.Debug(Localization.Logging.Result, countMessages);
            return countMessages;
        }
    }
}