using Microsoft.ML.Data;

namespace EvilNite.Modules.AntiSpamSystem;

/// <summary>
/// Represents the prediction result for spam detection.
/// </summary>
class SpamPrediction
{
    [ColumnName("PredictedLabel")]
    public string isSpam { get; set; }
}