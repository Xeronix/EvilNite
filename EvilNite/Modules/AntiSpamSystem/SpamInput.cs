using Microsoft.ML.Data;

namespace EvilNite.Modules.AntiSpamSystem;

/// <summary>
/// Represents the input data structure for spam detection.
/// </summary>
public class SpamInput
{
    [LoadColumn(0)]
    public string Label { get; set; }
    
    [LoadColumn(1)]
    public string Message { get; set; }
}