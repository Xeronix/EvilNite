using Dapper;
using EvilNite.Database;

namespace EvilNite.Modules.AntiSpamSystem;

/// <summary>
/// Manages user message indexing in the database.
/// </summary>
public class IndexUsers
{
    /// <summary>
    /// Indexes a user's message in the database, updating the message count or adding a new user record if necessary.
    /// </summary>
    /// <param name="memberId">The ID of the member whose message is being indexed.</param>
    public async Task IndexUserMessage(long memberId)
    {
        LoggerSetup.Log.Information(Localization.Logging.IndexUsers_RunIndexMessage);
        LoggerSetup.Log.Debug(Localization.Logging.MemberId, memberId);
        
        const string ifHaveIdQuery = "SELECT COUNT(1) FROM Members WHERE Id = @Id";

        using (var connection = ApplicationContext.GetConnection())
        { 
            connection.Open();
            var result = await connection.ExecuteScalarAsync<int>(ifHaveIdQuery, new { Id = memberId });
            LoggerSetup.Log.Debug(Localization.Logging.IndexUsers_HaveIdInDb, result);

            if (result == 0)
            {
                const string createUserQuery = "INSERT INTO Members VALUES (@Id, 0)";
                var createUser = await connection.ExecuteAsync(createUserQuery, new { Id = memberId });
                LoggerSetup.Log.Debug(Localization.Logging.IndexUsers_UserAddInDb, createUser);
            }

            const string countMessagesQuery = "SELECT CountMessages FROM Members WHERE Id = @Id";
            var countMessages = await connection.ExecuteScalarAsync<long>(countMessagesQuery, new { Id = memberId });
            LoggerSetup.Log.Debug(Localization.Logging.IndexUsers_CountUserMessages, countMessages);

            const string updateCountMessagesQuery = "UPDATE Members SET CountMessages = @NewCountMessages WHERE Id = @Id";
            var updateCountMessages = await connection.ExecuteAsync(updateCountMessagesQuery,
                new { NewCountMessages = ++countMessages, Id = memberId });
            LoggerSetup.Log.Debug(Localization.Logging.IndexUsers_UpdateCountMessages, updateCountMessages);
        }
    }
}
