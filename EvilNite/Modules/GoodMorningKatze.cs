using EvilNite.ConfigManager;
using EvilNite.Core;
using Telegram.Bot;

namespace EvilNite.Modules;

/// <summary>
/// This class is responsible for sending scheduled good morning and good night messages to @katze_942.
/// </summary>
public class GoodMorningKatze
{
    // Array of event times and corresponding messages
    private readonly (TimeSpan time, string message)[] _eventTimes =
    {
        (new TimeSpan(6, 0, 0), "Доброе утро @katze_942"),
        (new TimeSpan(20, 0, 0), "Спокойной ночи @katze_942"),
        
        // And for Yuta, he asked)
        
        (new TimeSpan(20, 0, 10), "Доброе утро @nsdkinx"),
        (new TimeSpan(11, 0, 0), "Спокойной ночи @nsdkinx")
    };

    /// <summary>
    /// Entry point of the Good Morning Katze. Waits for specific times of the day and sends corresponding messages.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    public async Task StartGoodMorning(ITelegramBotClient botClient)
    {
        LoggerSetup.Log.Information(Localization.Logging.GoodMorningKatze_Run);
    
        var chatId = ReadWriteConfig.ConfigModel.ChatId;

        while (true)
        {
            try
            {
                DateTime nowUtc = DateTime.UtcNow;
                var nextEvent = GetNextEvent(nowUtc);

                TimeSpan timeToWait = nextEvent.time - nowUtc.TimeOfDay;
                if (timeToWait < TimeSpan.Zero)
                {
                    timeToWait += TimeSpan.FromDays(1);
                }

                LoggerSetup.Log.Information(Localization.Logging.GoodMorningKatze_Waiting, nextEvent.time, nextEvent.message);
                LoggerSetup.Log.Debug(Localization.Logging.GoodMorningKatze_NextEventIn, timeToWait.TotalMinutes);

                await Task.Delay(timeToWait);

                LoggerSetup.Log.Information(Localization.Logging.GoodMorningKatze_SendingMessage, nextEvent.message);
                await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: nextEvent.message);
            }
            catch (Exception ex)
            {
                Helper.OutputErrorWithEx(ex);
            }
        }
    }

    /// <summary>
    /// Calculates the next event time and corresponding message in UTC.
    /// </summary>
    /// <param name="nowUtc">The current UTC date and time.</param>
    /// <returns>The next event time and corresponding message.</returns>
    private (TimeSpan time, string message) GetNextEvent(DateTime nowUtc)
    {
        var currentTime = nowUtc.TimeOfDay;
        var nextEvent = _eventTimes
            .Where(e => e.time > currentTime)
            .OrderBy(e => e.time)
            .FirstOrDefault();

        if (nextEvent == default)
        {
            LoggerSetup.Log.Debug(Localization.Logging.GoodMorningKatze_NoEventToday);
            nextEvent = _eventTimes.First();
        }

        return nextEvent;
    }
}