using GTranslate.Translators;
using LanguageDetection;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Helper = EvilNite.Core.Helper;

namespace EvilNite.Modules;

/// <summary>
/// This class is responsible for translating the message into Russian.
/// </summary>
public class TranslateMessage
{
    /// <summary>
    /// This method tells what language is in the message, used to check.
    /// </summary>
    /// <param name="text">Message Text</param>
    /// <returns>Language code: eng, rus, ukr</returns>
    public static string DetectLanguage(string text)
    {
        LoggerSetup.Log.Information(Localization.Logging.TranslateMessage_RunDetectLanguage);
        LoggerSetup.Log.Debug(Localization.Logging.Text, text);
        
        if (text != null)
        {
            var detectorLanguage = new LanguageDetector();
            detectorLanguage.AddLanguages(new[]
            {
                "eng", "rus", "ukr"
            });

            var detectLanguage = detectorLanguage.Detect(text);
            LoggerSetup.Log.Debug(Localization.Logging.TranslateMessage_Language, detectLanguage);
            
            return detectLanguage;
        }
        else
            return "rus";
    }

    /// <summary>
    /// Message translation handler.
    /// </summary>
    /// <param name="botClient">Telegram Bot Client</param>
    /// <param name="update">Telegram Bot Update</param>
    /// <param name="cancellationToken">Cancellation Token</param>
    public async Task Handler(ITelegramBotClient botClient, Telegram.Bot.Types.Update update, CancellationToken cancellationToken)
    {
        LoggerSetup.Log.Information(Localization.Logging.TranslateMessage_RunHandler);
        
        int messageId = update.Message.MessageId;
        var resultTranslate = await TranslateMessageText(update.Message.Text);

        if (resultTranslate != null)
        {
            try
            {
                await botClient.SendTextMessageAsync(
                    chatId: update.Message.Chat.Id,
                    text: resultTranslate,
                    replyToMessageId: messageId);
            }
            catch (ApiRequestException ex)
            {
                Helper.OutputErrorWithEx(ex, Localization.Logging.TranslateMessage_ErrorIgnor);
            }
        }
    }

    /// <summary>
    /// Translates the provided text into Russian.
    /// </summary>
    /// <param name="text">Text to be translated</param>
    /// <returns>Translated text in Russian</returns>
    private async Task<string?> TranslateMessageText(string text)
    {
        LoggerSetup.Log.Information(Localization.Logging.TranslateMessage_RunTranslateMessageText);
        LoggerSetup.Log.Debug(Localization.Logging.Text, text);
        
        try
        {
            var translator = new YandexTranslator();
            var result = await translator.TranslateAsync(text, "ru");
        
            bool ifReturnTranslate = result.SourceLanguage != result.TargetLanguage && result.Translation != text 
                && !Helper.IsValidUrl(text) // Is URL
                && !Helper.IsTelegramUsername(text) // Is @username
                && !Helper.IsCommand(text) // Is Command
                && !Helper.IsEmoji(text); // Is Emoji
            
            LoggerSetup.Log.Debug(Localization.Logging.TranslateMessage_ReturnTranslateResult, result.Translation, ifReturnTranslate);
            
            if (ifReturnTranslate)
                return result.Translation;
            else
                return null;
        }
        catch (HttpRequestException exception)
        {
            Helper.OutputErrorWithEx(exception);
            return null;
        }
    }
}
