
using System.Text.RegularExpressions;

namespace EvilNite.Core;

/// <summary>
/// In this class there are methods that can not be taken to other classes and create a class for the sake of 1 small method is not desirable (someday methods from here will be moved to special classes).
/// </summary>
public class Helper
{
    /// <summary>
    /// This method is used for more convenient error output (I will add logging in the future)
    /// </summary>
    /// <param name="exc">Exception object</param>
    /// <param name="additionalOutput">Additional output for an error (e.g. its solution)</param>
    public static void OutputErrorWithEx(Exception exc, string additionalOutput = null)
    {
        LoggerSetup.Log.Error(Localization.Logging.Helper_ErrorLog, additionalOutput, exc);
    }
    
    /// <summary>
    /// This method checks if a given string is a valid URL.
    /// </summary>
    /// <param name="text">The string to be checked</param>
    /// <returns>True if the string is a valid URL with a domain, otherwise false</returns>
    public static bool IsValidUrl(string text)
    {
        LoggerSetup.Log.Information(Localization.Logging.Helper_RunTextValidation, "URL");
        LoggerSetup.Log.Debug(Localization.Logging.Text, text);
        
        if (string.IsNullOrWhiteSpace(text))
            return false;

        if (!Uri.TryCreate(text, UriKind.Absolute, out Uri uriResult))
            return false;

        bool isUrl = uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps && !string.IsNullOrWhiteSpace(uriResult.Host);
        
        LoggerSetup.Log.Debug(Localization.Logging.Result, isUrl);
        return isUrl;
    }


    /// <summary>
    /// This method checks if the text username is the username from telegram
    /// </summary>
    /// <param name="text">Text to be checked</param>
    /// <returns>Returns True if this is Username</returns>
    public static bool IsTelegramUsername(string text)
    {
        LoggerSetup.Log.Information(Localization.Logging.Helper_RunTextValidation, "Username");
        LoggerSetup.Log.Debug(Localization.Logging.Text, text);
        
        if (string.IsNullOrWhiteSpace(text))
            return false;

        // Regex pattern to match Telegram usernames starting with @ and containing only valid characters
        string pattern = @"^@[a-zA-Z0-9_]{5,32}$";
        bool isUsername = Regex.IsMatch(text, pattern);
        
        LoggerSetup.Log.Debug(Localization.Logging.Result, isUsername);
        return isUsername;
    }

    /// <summary>
    /// This method checks whether the command or not
    /// </summary>
    /// <param name="text">Text to be checked</param>
    /// <returns>Returns True if the command</returns>
    public static bool IsCommand(string text)
    {
        LoggerSetup.Log.Information(Localization.Logging.Helper_RunTextValidation, "command");
        LoggerSetup.Log.Debug(Localization.Logging.Text, text);
        bool isCommand = text.TrimStart()[0] == '/';
        LoggerSetup.Log.Debug(Localization.Logging.Result, isCommand);
        return isCommand;
    }
    
    /// <summary>
    /// This method checks if the text is an emoji
    /// </summary>
    /// <param name="text">Text to be checked</param>
    /// <returns>Returns True if the text is an emoji</returns>
    public static bool IsEmoji(string text)
    {
        LoggerSetup.Log.Information(Localization.Logging.Helper_RunTextValidation, "emoji");
        LoggerSetup.Log.Debug(Localization.Logging.Text, text);
        
        if (string.IsNullOrWhiteSpace(text))
            return false;

        // Regex pattern to match emojis
        string pattern = @"^[\uD83C-\uDBFF\uDC00-\uDFFF]+$";
        bool isEmoji = Regex.IsMatch(text, pattern);
        
        LoggerSetup.Log.Debug(Localization.Logging.Result, isEmoji);
        return isEmoji;
    }
}