﻿using Telegram.Bot;
using Telegram.Bot.Types;

namespace EvilNite.Core.Commands;

/// <summary>
/// This class is responsible for muting and unmuting users
/// </summary>
public class Mute
{
    /// <summary>
    /// Processes the message and determines whether to mute or unmute a user.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="update">Update object containing message data.</param>
    /// <param name="param">Command parameters, such as "mute" or "unmute".</param>
    public async Task Handler(ITelegramBotClient botClient, Update update, string[] param)
    {
        LoggerSetup.Log.Information(Localization.Logging.Mute_RunHandler);
        LoggerSetup.Log.Debug(Localization.Logging.BanMute_HandlerDebugParam, update.Message.Text, string.Join(", ", param));
        
        if (param.Length == 0) return;

        long? id = await CommandHelper.GetIdUser(botClient, update, param);
        if (id == null) return;

        long chatId = update.Message.Chat.Id;
        bool isAdminOrCreator = await IsAdminOrCreator(botClient, update);

        if (isAdminOrCreator)
        {
            if (param[0] == "mute")
                await MuteMember(botClient, param, id.Value, chatId);
            else if (param[0] == "unmute")
                await UnmuteMember(botClient, chatId, id.Value);
        }
    }

    /// <summary>
    /// Checks if the user is an admin or the creator of the chat.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="update">Update object containing message data.</param>
    /// <returns>Returns true if the user is an admin or the creator of the chat, otherwise false.</returns>
    private async Task<bool> IsAdminOrCreator(ITelegramBotClient botClient, Update update)
    {
        long userId = update.Message.From.Id;
        bool isAdmin = Convert.ToBoolean(await CommandHelper.CheckIsAdmin(botClient, update, userId));
        bool isCreator = Convert.ToBoolean(await CommandHelper.CheckIsCreator(botClient, update, userId));

        return isAdmin || isCreator;
    }

    /// <summary>
    /// Mutes the specified user in the chat.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="param">Parameters with mute duration.</param>
    /// <param name="chatId">ID of the chat where the user will be muted.</param>
    /// <param name="userId">ID of the user to be muted.</param>
    public async Task MuteMember(ITelegramBotClient botClient, string[] param, long chatId, long userId)
    {
        LoggerSetup.Log.Information(Localization.Logging.Mute_RunMuteMember);
        LoggerSetup.Log.Debug(Localization.Logging.Mute_MuteMemberDebugParam, string.Join(", ", param), 
            userId, chatId);
        
        var chatPermissions = new ChatPermissions
        {
            CanSendMessages = false,
            CanSendOtherMessages = false,
            CanAddWebPagePreviews = false,
            CanSendVideoNotes = false,
            CanSendAudios = false,
            CanSendVideos = false,
            CanSendPhotos = false,
            CanSendPolls = false,
            CanSendDocuments = false,
            CanSendVoiceNotes = false
        };

        try
        {
            DateTime timeForMute = CommandHelper.ParseTime(param);
            await botClient.RestrictChatMemberAsync(chatId, userId, chatPermissions, untilDate: timeForMute);
            
            await botClient.SendTextMessageAsync(chatId, string.Format(Localization.Logging.Mute_MuteUserMessage, timeForMute));
            LoggerSetup.Log.Information(string.Format(Localization.Logging.Mute_MuteUserMessage, timeForMute));
        }
        catch (ArgumentException)
        {
            await botClient.SendTextMessageAsync(chatId, Localization.Logging.Mute_MuteUserError);
            LoggerSetup.Log.Warning(Localization.Logging.Mute_MuteUserError);
        }
    }

    /// <summary>
    /// Unmutes the specified user in the chat.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="chatId">ID of the chat where the user will be unmuted.</param>
    /// <param name="userId">ID of the user to be unmuted.</param>
    public async Task UnmuteMember(ITelegramBotClient botClient, long chatId, long userId)
    {
        LoggerSetup.Log.Information(Localization.Logging.Mute_RunUnmuteMember);
        LoggerSetup.Log.Debug(Localization.Logging.Mute_UnmuteMemberDebugParam, chatId, userId);
        
        var chatPermissions = new ChatPermissions
        {
            CanSendMessages = true,
            CanSendOtherMessages = true,
            CanAddWebPagePreviews = true,
            CanSendVideoNotes = true,
            CanSendAudios = true,
            CanSendVideos = true,
            CanSendPhotos = true,
            CanSendPolls = true,
            CanSendDocuments = true,
            CanSendVoiceNotes = true
        };

        await botClient.RestrictChatMemberAsync(chatId, userId, chatPermissions);
        await botClient.SendTextMessageAsync(chatId, Localization.Logging.Mute_UnmuteMemberMessage);
    }
}