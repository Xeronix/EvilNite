using Telegram.Bot;
using Telegram.Bot.Types;

namespace EvilNite.Core.Commands;

/// <summary>
/// This class is responsible for banning and unbanning users
/// </summary>
public class Ban
{
    private readonly long _chatId = Convert.ToInt64(ConfigManager.ReadWriteConfig.ConfigModel.ChatId);

    /// <summary>
    /// Processes the message and determines whether to ban or unban a user.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="update">Update object containing message data.</param>
    /// <param name="param">Command parameters, such as "/ban" or "/unban".</param>
    public async Task Handler(ITelegramBotClient botClient, Update update, string[] param)
    {
        LoggerSetup.Log.Information(Localization.Logging.Ban_RunHandler);
        LoggerSetup.Log.Debug(Localization.Logging.BanMute_HandlerDebugParam, update.Message.Text, string.Join(", ", param));
        
        if (param.Length == 0) return;

        long? id = await CommandHelper.GetIdUser(botClient, update, param);
        if (id == null) return;

        bool isAdminOrCreator = await IsAdminOrCreator(botClient, update);

        if (isAdminOrCreator)
        {
            if (param[0] == "/ban")
                await BanMember(botClient, id);
            else if (param[0] == "/unban")
                await UnbanMember(botClient, id);
        }
    }

    /// <summary>
    /// Checks if the user is an admin or the creator of the chat.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="update">Update object containing message data.</param>
    /// <returns>Returns true if the user is an admin or the creator of the chat, otherwise false.</returns>
    private async Task<bool> IsAdminOrCreator(ITelegramBotClient botClient, Update update)
    {
        long userId = update.Message.From.Id;
        bool isAdmin = Convert.ToBoolean(await CommandHelper.CheckIsAdmin(botClient, update, userId));
        bool isCreator = Convert.ToBoolean(await CommandHelper.CheckIsCreator(botClient, update, userId));

        return isAdmin || isCreator;
    }

    /// <summary>
    /// Bans the specified user from the chat.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="userId">ID of the user to be banned.</param>
    public async Task BanMember(ITelegramBotClient botClient, long? userId)
    {
        LoggerSetup.Log.Information(Localization.Logging.Ban_RunBanMember);
        LoggerSetup.Log.Debug(Localization.Logging.UserId, userId);
        
        if (userId == null) return;
        try
        {
            await botClient.BanChatMemberAsync(_chatId, userId.Value);
            LoggerSetup.Log.Information(Localization.Logging.Ban_BanMessage);

            await botClient.SendTextMessageAsync(_chatId, Localization.Logging.Ban_BanMessage);
        }
        catch (Telegram.Bot.Exceptions.ApiRequestException ex) when (ex.Message.Contains("user is an administrator of the chat"))
        {
            await botClient.SendTextMessageAsync(_chatId, Localization.Logging.Ban_ErrorBanMemberAdmin);
        }
    }

    /// <summary>
    /// Unbans the specified user from the chat.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="userId">ID of the user to be unbanned.</param>
    public async Task UnbanMember(ITelegramBotClient botClient, long? userId)
    {
        LoggerSetup.Log.Information(Localization.Logging.Ban_RunUnbanMember);
        LoggerSetup.Log.Debug(Localization.Logging.UserId, userId);
        
        if (userId == null) return;

        await botClient.UnbanChatMemberAsync(_chatId, userId.Value);
        await botClient.SendTextMessageAsync(_chatId, Localization.Logging.Ban_UnbanMessage);
    }
}