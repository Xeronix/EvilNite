﻿using EvilNite.ConfigManager;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace EvilNite.Core.Commands;

/// <summary>
/// This class provides helper methods for various command-related operations.
/// </summary>
public class CommandHelper
{
    /// <summary>
    /// Retrieves the user ID based on the provided message or command parameters.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="update">Update object containing message data.</param>
    /// <param name="param">Command parameters, including potential user ID.</param>
    /// <returns>Returns the user ID if found, otherwise null.</returns>
    public static async Task<long?> GetIdUser(ITelegramBotClient botClient, Update update, string[] param)
    {
        LoggerSetup.Log.Information(Localization.Logging.CommandHelper_RunGetIdUser);
        LoggerSetup.Log.Debug(Localization.Logging.CommandHelper_DebugParam, string.Join(", ", param));

        var messageObj = update.Message;

        // Return the ID if the message is a reply
        var userId = messageObj?.ReplyToMessage?.From?.Id;
        if (userId != null)
        {
            LoggerSetup.Log.Information(Localization.Logging.UserId, userId);
            return userId;
        }

        // Otherwise, try to get the user ID from the provided parameter
        try
        {
            var chatMember = await botClient.GetChatMemberAsync(messageObj.Chat.Id, Convert.ToInt64(param[1]));
            LoggerSetup.Log.Information(Localization.Logging.UserId, chatMember.User.Id);
            return chatMember.User.Id;
        }
        catch (Exception ex)
        {
            LoggerSetup.Log.Warning(Localization.Logging.CommandHelper_ErrorGettingUserId, ex);
            try
            {
                userId = messageObj?.From.Id;
                return userId;
            }
            catch (Exception e)
            {
                Helper.OutputErrorWithEx(e);
                throw;
            }
        }
    }

    /// <summary>
    /// Checks whether the specified user is an admin in the chat.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="update">Update object containing message data.</param>
    /// <param name="id">User ID to check.</param>
    /// <returns>Returns true if the user is an admin, false if not, or null if the user is not found.</returns>
    public static async Task<bool?> CheckIsAdmin(ITelegramBotClient botClient, Update update, long id)
    {
        LoggerSetup.Log.Information(Localization.Logging.CommandHelper_RunCheckIsAdmin);
        LoggerSetup.Log.Debug(Localization.Logging.UserId, id);

        try
        {
            //update.CallbackQuery.From.Id   

            long chatId = Convert.ToInt64(ReadWriteConfig.ConfigModel.ChatId);

            var chatMember = await botClient.GetChatMemberAsync(chatId, id);
            LoggerSetup.Log.Information(Localization.Logging.CommandHelper_UserStatus, chatMember.Status);
            
            return chatMember.Status == ChatMemberStatus.Administrator;
        }
        catch (Exception ex)
        {
            Helper.OutputErrorWithEx(ex, string.Format(Localization.Logging.CommandHelper_ErrorCheckIsAdmin, ex));
            return null;
        }
    }

    /// <summary>
    /// Checks whether the specified user is the creator of the chat.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="update">Update object containing message data.</param>
    /// <param name="id">User ID to check.</param>
    /// <returns>Returns true if the user is the creator, false if not, or null if the user is not found.</returns>
    public static async Task<bool?> CheckIsCreator(ITelegramBotClient botClient, Update update, long id)
    {
        LoggerSetup.Log.Information(Localization.Logging.CommandHelper_RunCheckIsCreator);
        LoggerSetup.Log.Debug(Localization.Logging.UserId, id);

        try
        {
            long chatId = Convert.ToInt64(ReadWriteConfig.ConfigModel.ChatId);

            var chatMember = await botClient.GetChatMemberAsync(chatId, id);
            LoggerSetup.Log.Information(Localization.Logging.CommandHelper_UserStatus, chatMember.Status);
            return chatMember.Status == ChatMemberStatus.Creator;
        }
        catch (Exception ex)
        {
            LoggerSetup.Log.Error(Localization.Logging.CommandHelper_ErrorCheckIsCreator, ex);
            return null;
        }
    }

    /// <summary>
    /// Parses a time string from the command parameters and returns the corresponding DateTime.
    /// </summary>
    /// <param name="param">Command parameters, including the time string to parse.</param>
    /// <returns>Returns the DateTime corresponding to the parsed time string.</returns>
    /// <exception cref="ArgumentException">Thrown when the time string cannot be parsed.</exception>
    public static DateTime ParseTime(string[] param)
    {
        LoggerSetup.Log.Information(Localization.Logging.CommandHelper_RunParseTime);
        LoggerSetup.Log.Debug(Localization.Logging.CommandHelper_DebugParam, string.Join(", ", param));

        string timeString = param[1];

        var intervalMap = new Dictionary<char, Func<int, TimeSpan>>
        {
            { 's', seconds => TimeSpan.FromSeconds(seconds) },
            { 'm', minutes => TimeSpan.FromMinutes(minutes) },
            { 'h', hours => TimeSpan.FromHours(hours) },
            { 'd', days => TimeSpan.FromDays(days) },
            { 'w', weeks => TimeSpan.FromDays(weeks * 7) },
            { 'y', years => TimeSpan.FromDays(years * 365) }
        };

        try
        {
            if (timeString.Length >= 2 && intervalMap.TryGetValue(timeString[^1], out var intervalFunction))
            {
                if (int.TryParse(timeString[..^1], out int value))
                {
                    DateTime newTime = DateTime.UtcNow.Add(intervalFunction(value));
                    LoggerSetup.Log.Information(Localization.Logging.Result, newTime);
                    return newTime;
                }

                LoggerSetup.Log.Error(Localization.Logging.CommandHelper_ErrorConvertingTime);
                throw new ArgumentException("Error: Invalid numeric value.");
            }

            LoggerSetup.Log.Error(Localization.Logging.CommandHelper_ErrorConvertingTime);
            throw new ArgumentException("Error: Invalid time format.");
        }
        catch (Exception ex)
        {
            LoggerSetup.Log.Error(Localization.Logging.CommandHelper_ErrorConvertingTime, ex);
            throw;
        }
    }
}