using EvilNite.Modules.AntiSpamSystem;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace EvilNite.Core.Commands;

public class AddToSpamDb
{
    /// <summary>
    /// Handles the addition of a message to the spam database and reloads the model with the new data.
    /// </summary>
    /// <param name="botClient">Instance of the Telegram bot client.</param>
    /// <param name="update">Update object containing the message data.</param>
    /// <param name="param">Command parameters, including the spam message.</param>
    public async Task Handler(ITelegramBotClient botClient, Update update, string[] param)
    {
        LoggerSetup.Log.Information(Localization.Logging.AddToSpamDb_RunHandler);
        LoggerSetup.Log.Debug(Localization.Logging.AddToSpamDb_HandlerDebugParam, update.Message.Text, string.Join(", ", param));

        long? id = await CommandHelper.GetIdUser(botClient, update, param);
        if (id == null) 
        {
            LoggerSetup.Log.Warning(Localization.Logging.AddToSpamDb_UserIdNotFound);
            return;
        }

        bool isAdminOrCreator = await IsAdminOrCreator(botClient, update);

        if (isAdminOrCreator)
        {
            switch (param[0])
            {
                case "/spam":
                    await Spam(param, update, botClient);
                    break;
            }
        }
        else
        {
            LoggerSetup.Log.Warning(Localization.Logging.AddToSpamDb_UserNotAuthorized);
        }
    }

    private async Task Spam(string[] param, Update update, ITelegramBotClient botClient)
    {
        string messageText = string.Empty;
        if (param.Length >= 2)
            messageText = string.Join(" ", param.Skip(1)); // Combine the message parts
        else if (param.Length == 1 && update.Message.ReplyToMessage != null)
            messageText = update.Message.ReplyToMessage.Text;
        else
        {
            LoggerSetup.Log.Warning(Localization.Logging.AddToSpamDb_InvalidParameters);
            return;
        }
            
        LoggerSetup.Log.Information(Localization.Logging.AddToSpamDb_AddingMessageToSpamDb, messageText);

        try
        {
            await AntiSpam.AddSpamHamMessage(true, messageText); // Add message as spam to the database
            LoggerSetup.Log.Information(Localization.Logging.AddToSpamDb_MessageAddedToSpamDb);

            AntiSpam.TrainingSpamDetector(); // Retrain the model with new data
            LoggerSetup.Log.Information(Localization.Logging.AddToSpamDb_ModelRetrained);

            await botClient.SendTextMessageAsync(update.Message.Chat.Id, Localization.Logging.AddToSpamDb_ConfirmationMessage, replyToMessageId: update.Message.MessageId);
        }
        catch (Exception ex)
        {
            Helper.OutputErrorWithEx(ex, Localization.Logging.AddToSpamDb_InvalidParameters);
            throw;
        }
    }

    /// <summary>
    /// Checks if the user is an admin or the creator of the chat.
    /// </summary>
    /// <param name="botClient">Instance of the bot client.</param>
    /// <param name="update">Update object containing message data.</param>
    /// <returns>Returns true if the user is an admin or the creator of the chat, otherwise false.</returns>
    private async Task<bool> IsAdminOrCreator(ITelegramBotClient botClient, Update update)
    {
        long userId = update.Message.From.Id;
        bool isAdmin = Convert.ToBoolean(await CommandHelper.CheckIsAdmin(botClient, update, userId));
        bool isCreator = Convert.ToBoolean(await CommandHelper.CheckIsCreator(botClient, update, userId));

        return isAdmin || isCreator;
    }
}