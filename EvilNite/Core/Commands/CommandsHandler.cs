using Telegram.Bot;
using Telegram.Bot.Types;

namespace EvilNite.Core.Commands;

/// <summary>
/// This class is responsible for processing commands
/// </summary>
public class CommandsHandler
{
    /// <summary>
    /// This method processes commands and then redirects them to other command handlers (e.g. /ban)
    /// </summary>
    /// <param name="botClient"></param>
    /// <param name="update"></param>
    /// <param name="cancellationToken"></param>
    public async Task Handler(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        LoggerSetup.Log.Information(Localization.Logging.CommandsHandler_RunHandler);
        LoggerSetup.Log.Debug(Localization.Logging.CommandsHandler_HandlerDebugUserMessage, update.Message.Text, update.Message.From.Id);
        var param = update.Message.Text.Split(" ")[0].Split("@");
        LoggerSetup.Log.Debug(Localization.Logging.CommandsHandler_HandlerParamDebug, string.Join(", ", param));

        Ban ban = new Ban();
        Mute mute = new Mute();

        if (param.Length > 1 && await CheckBotCall(param, botClient) // The first condition checks if the bot has been accessed (example: /ban@UserNameBot).
            || param.Length == 1)                                          // If not, the second condition if a simple command is entered (example: /ban). In this way only commands with bot mention and simple commands are processed
        {
            switch (param[0])
            {
                case "/spam":
                    AddToSpamDb addToSpamDb = new AddToSpamDb();
                    await addToSpamDb.Handler(botClient, update, update.Message.Text.Split(" "));
                    break;
            }
            /*switch (param[0])
            {
                case "/ban":
                case "/unban":
                    await ban.Handler(botClient, update, param);
                    break;
                case "/mute":
                case "/unmute":
                    await mute.Handler(botClient, update, param);
                    break;
            }*/
        }
    }

    /// <summary>
    /// This method checks whether the command is addressed to the bot or not
    /// </summary>
    /// <param name="text">Command text</param>
    /// <param name="botClient">Bot object</param>
    /// <returns>Returns True if it matches</returns>
    private async Task<bool> CheckBotCall(string[] text, ITelegramBotClient botClient)
    {
        LoggerSetup.Log.Information(Localization.Logging.CommandsHandler_RunCheckBotCall);
        
        var botMe = await botClient.GetMeAsync();
        var username = botMe.Username;
        LoggerSetup.Log.Debug(Localization.Logging.CommandsHandler_BotUsername, username);

        return username == text[1];
    }
}