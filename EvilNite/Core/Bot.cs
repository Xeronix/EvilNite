using EvilNite.Localization.ConfigManager;
using EvilNite.Modules;
using EvilNite.Modules.AntiSpamSystem;
using Telegram.Bot;
using Telegram.Bot.Polling;

namespace EvilNite.Core
{
    /// <summary>
    /// This class represents the main bot instance and its configuration.
    /// </summary>
    public class Bot
    {
        private static ITelegramBotClient _botClient = null!;

        /// <summary>
        /// Starts the bot and initializes its configuration.
        /// </summary>
        /// <returns>A task representing the bot startup process.</returns>
        public async Task StartBot()
        {
            _botClient = new TelegramBotClient(ConfigManager.ReadWriteConfig.ConfigModel.BotToken);
            
            // Start AntiSpam system
            AntiSpam.TrainingSpamDetector();
            
            var cts = new CancellationTokenSource();
            var cancellationToken = cts.Token;
            var receiverOptions = new ReceiverOptions { };
            _botClient.StartReceiving(
                Handlers.Update.HandleUpdateAsync,
                Handlers.Error.HandlePollingErrorAsync,
                receiverOptions,
                cancellationToken
            );

            var me = await _botClient.GetMeAsync(); // Method has overload with cancellation support
            Console.WriteLine(Localization.Core.Bot.Bot_Hi + me.Username);

            var goodMorningKatze = new GoodMorningKatze();
            await Task.Run(() =>
            {
                goodMorningKatze.StartGoodMorning(_botClient);
            }, cancellationToken);

            // Stopping the bot when any button is pressed
            Console.WriteLine("\n" + Localization.Core.Bot.Bot_PressAnyKey);
            Console.ReadKey();
            cts.Cancel();
        }
    }
}