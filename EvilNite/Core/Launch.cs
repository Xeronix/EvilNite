using EvilNite.ConfigManager;
using EvilNite.ConfigManager.Config;
using EvilNite.Database;

namespace EvilNite.Core;

/// <summary>
/// This class is responsible for launching the bot and possible configuration on first run
/// </summary>
public class Launch
{
    /// <summary>
    /// This method runs methods to create reading the Toml config and creating tables in the database
    /// </summary>
    public async Task LaunchBot()
    {
        ReadTomlFile();
        
        // Creating a basic bot framework in the database if it is missing
        Migration upDatabase = new Migration();
        upDatabase.Up();
        
        // Launching the bot (finally) Thièl ana ing kéné
        var botObject = new Bot();
        await botObject.StartBot();
    }
    
    /// <summary>
    /// This method checks if the Toml config is in place and reconfigures if it is missing or reads an existing one.
    /// </summary>
    private void ReadTomlFile()
    {
        LoggerSetup.Log.Debug(Localization.Logging.Launch_ReadTomlFile);
        var readWriteConfig = new ReadWriteConfig();
        var createBaseConfig = new CreateBase();

        var configExists = File.Exists(GlobalConfig.PathToConfApp);
        LoggerSetup.Log.Debug(Localization.Logging.Launch_ConfigExists, GlobalConfig.PathToConfApp, configExists);
        if (configExists)
            readWriteConfig.ReadConfig();
        else
            createBaseConfig.Config();
    }
}