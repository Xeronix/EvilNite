using Telegram.Bot;
using Telegram.Bot.Exceptions;

namespace EvilNite.Core.Handlers
{
    /// <summary>
    /// This class handles errors that may occur during polling.
    /// </summary>
    public class Error
    {
        /// <summary>
        /// Handles polling errors and logs them.
        /// </summary>
        /// <param name="botClient">The Telegram Bot Client instance.</param>
        /// <param name="exception">The exception that occurred.</param>
        /// <param name="cancellationToken">The cancellation token for handling the operation.</param>
        /// <returns>A completed task.</returns>
        public static Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            // Determine the error message based on the exception type.
            var ErrorMessage = exception switch
            {
                ApiRequestException apiRequestException
                    => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            // Log the error message.
            Helper.OutputErrorWithEx(exception, ErrorMessage);

            // Return a completed task.
            return Task.CompletedTask;
        }
    }
}