using EvilNite.Core.Commands;
using EvilNite.Modules.AntiSpamSystem;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace EvilNite.Core.Handlers
{
    /// <summary>
    /// This class handles updates received from Telegram.
    /// </summary>
    public class Update
    {
        /// <summary>
        /// Handles incoming updates and routes them to the appropriate handlers.
        /// </summary>
        /// <param name="botClient">The Telegram Bot Client instance.</param>
        /// <param name="update">The Telegram Bot Update received.</param>
        /// <param name="cancellationToken">The cancellation token for handling the operation.</param>
        public static async Task HandleUpdateAsync(ITelegramBotClient botClient, Telegram.Bot.Types.Update update, CancellationToken cancellationToken)
        {
            LoggerSetup.Log.Information(Localization.Logging.Update_UpdateType, update.Type);
            switch (update.Type)
            {
                case UpdateType.Message:
                    _ = Task.Run(() => HandlerMessage(botClient, update, cancellationToken));
                    break;
                case UpdateType.CallbackQuery:
                    _ = Task.Run(() => HandlerCallbackQuery(botClient, update, cancellationToken));
                    break;
            }
        }

        /// <summary>
        /// This method processes messages by calling other handlers, e.g. English text goes to the TranslateMessage handler
        /// </summary>
        /// <param name="botClient"></param>
        /// <param name="update"></param>
        /// <param name="cancellationToken"></param>
        private static async Task HandlerMessage(ITelegramBotClient botClient, Telegram.Bot.Types.Update update, CancellationToken cancellationToken)
        {
            var messageObj = update.Message;
            LoggerSetup.Log.Information(Localization.Logging.Update_MessageReceived, DateTime.Now, messageObj.From, messageObj.Chat.Id, messageObj.Text);

            var checkForSpam = await AntiSpam.MessageEvaluation(botClient, update);
            if (checkForSpam == false)
            {
                var indexUser = new IndexUsers();
                _ = Task.Run(() => indexUser.IndexUserMessage(messageObj.From.Id));
            
                if (!string.IsNullOrEmpty(messageObj.Text) && Helper.IsCommand(messageObj.Text))
                {
                    var commandHandler = new CommandsHandler();
                    _ = Task.Run(() => commandHandler.Handler(botClient, update, cancellationToken));
                }
                else if (Modules.TranslateMessage.DetectLanguage(messageObj.Text) != "rus")
                {
                    var translateModule = new Modules.TranslateMessage();
                    _ = Task.Run(() => translateModule.Handler(botClient, update, cancellationToken));
                }
            }
        }

        private static async Task HandlerCallbackQuery(ITelegramBotClient botClient, Telegram.Bot.Types.Update update,
            CancellationToken cancellationToken)
        {
            var jsonData = JsonConvert.DeserializeObject<CallbackData>(update.CallbackQuery.Data);
            
            var checkIsAdmin = Convert.ToBoolean(await CommandHelper.CheckIsAdmin(botClient, update, update.CallbackQuery.From.Id));
            var checkIsCreator = Convert.ToBoolean(await CommandHelper.CheckIsCreator(botClient, update, update.CallbackQuery.From.Id));
            var check = checkIsAdmin || checkIsCreator;

            if (check)
            {
                switch (jsonData.Type)
                {
                    case "SpamMessage":
                        await AntiSpam.ThisSpamMessage(botClient, Convert.ToInt32(jsonData.MessageId));
                        break;
                    case "NotSpamMessage":
                        await AntiSpam.ThisNotSpamMessage(botClient, Convert.ToInt32(jsonData.MessageId));
                        break;
                    case "ShowSpamMessage":
                        await AntiSpam.ShowSpamMessage(botClient, update.CallbackQuery.From.Id, jsonData.MessageId);
                        break;
                }
            }
        }
    }
}