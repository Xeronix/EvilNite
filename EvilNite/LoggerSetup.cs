using Serilog;
using Serilog.Events;

namespace EvilNite;

public static class LoggerSetup
{
    public static readonly ILogger Log;

    static LoggerSetup()
    {
        Log = new LoggerConfiguration()
            .WriteTo.Console()
            .WriteTo.File("evilnite.logs",
                rollingInterval: RollingInterval.Day)
            .CreateLogger();
    }
}